#this is the programme for creating your submission document
#I've edited this code so it prints out my results into a .tex document - I hope this what I was meant to do!


from math import *
import random as rnd
import numpy as np


class Submission:
    def __init__(self,filename):
        self.document=open(filename+".tex",'w')
        self.segment_direction={6:"(0,0)--(1,0)",5:"(1.05,0.05)--(1.05,1.05)",4:"(-.05,0.05)--(-.05,1.05)",3:"(0,1.10)--(1,1.10)",2:"(1.05,1.15)--(1.05,2.15)",1:"(-.05,1.15)--(-.05,2.15)",0:"(0,2.20)--(1,2.20)"}

        
    def header(self,candidate_name):
        self.document.write("\\documentclass[a4paper,fontsize=12pt]{scrartcl}\n")
        self.document.write("\\usepackage{tikz}\n")
        self.document.write("\\begin{document}\n")
        self.document.write("\\section*{"+candidate_name+"}\n")

    def section(self,section_name):
        self.document.write("\\subsection*{"+section_name+"}\n")
        
    def matrix_print(self,matrix_name,matrix,rounding=2):
        n=len(matrix)
        r=len(matrix[0])
        self.document.write("$$ "+matrix_name+"=\\left(\\begin{array}{")
        for i in range(0,r):
            self.document.write("c")
        self.document.write("}\n")
        for j in range(0,n):
            for i in range(0,r):
                self.document.write(str(round(matrix[j][i],rounding)))
                if i!=r-1:
                    self.document.write("&")
            self.document.write("\\\\")
        self.document.write("\\end{array}\\right)$$\n")

        
    def seven_segment_display(self,pattern):
        
        self.document.write("\\begin{tikzpicture}\n")
    
        def write_draw(segment_status):

            if segment_status==1:
                self.document.write("\\draw[ultra thick,color=red]")
            else:
                self.document.write("\\draw[thick,color=lightgray]")


        for segment,status in enumerate(pattern):
            write_draw(status)
            self.document.write(self.segment_direction[segment])
            self.document.write(";\n")
        
        self.document.write("\\end{tikzpicture}\n")


    def to_bool(self,a):
        if a==1:
            return True
        return False

    def seven_segment(self,pattern):
        self.seven_segment_display(pattern[0:7])
        number=0
        for i,b in enumerate(map(self.to_bool,pattern[7:11])):
            if b:
                number+=pow(2,i)
        self.document.write(str(int(number))+"\n")

    def cr(self):
        self.document.write("\\\\")

    def print_number(self,number,rounding=3):
        self.document.write(" "+str(round(number,rounding))+" ")

        
    def qquad(self):
        self.document.write("\\qquad\n ")

        
    def bottomer(self):
        self.document.write("\\end{document}")
        self.document.close()


def weight_matrix(vec):
    matrix = np.zeros((len(vec),len(vec)))
    for i in range(0,len(vec)):
        for j in range(0,len(vec)):
            if i == j:
                matrix[i][j] = 0
            else:
                matrix[i][j] = vec[i]*vec[j]
    return matrix

def mcculluchpitts(vec, W_matrix):
    new_vec = np.zeros((1,len(vec)))
    for i in range(0,len(vec)):
        new_vec[0][i] = np.sign(np.dot(W_matrix[i][:], vec))
    x = new_vec - vec
    return new_vec

if __name__ == '__main__':


    matrix=[[(i+j)/7.0 for j in range(0,4)] for i in range(0,3)]
    w_one = weight_matrix([-1,-1,1,-1,-1,1,-1,1,-1,-1,-1])
    w_three = weight_matrix([1,-1,1,1,-1,1,1,1,1,-1,-1])
    w_six = weight_matrix([1,1,-1,1,1,1,1,-1,1,1,-1])
    W = (1/3)*(w_one + w_three + w_six)
    six=[1,1,-1,1,1,1,1,-1,1,1,-1]
    three=[1,-1,1,1,-1,1,1,1,1,-1,-1]
    one=[-1,-1,1,-1,-1,1,-1,1,-1,-1,-1]
    test1 =[1,-1,1,1,-1,1,1,-1,-1,-1,-1]
    test2 =[1,1,1,1,1,1,1,-1,-1,-1,-1]
    
    submission=Submission("Alfred_Brown")
    
    submission.header("Alfred Brown, ab16230, Candidate Number: 39432")
    
    submission.section("Weight matrix")
    #submission.matrix_print("C",matrix)
    submission.matrix_print("W", W)

    submission.section("Training patterns")
    submission.seven_segment(six)
    submission.qquad()
    submission.seven_segment(three)
    submission.qquad()
    submission.seven_segment(one)

    submission.section("Evolving first test vector (test 1)")

    """ test_pattern=six[:]
    
    for i in range(0,10):
        submission.seven_segment(test_pattern)
        submission.qquad()
        if (i%6)==5:
            submission.cr()
            submission.cr()
        change=rnd.randint(0,len(test_pattern)-1)
        test_pattern[change]*=-1 """

    submission.seven_segment(test1)
    j = 1
    while j < 2:
        test1 = mcculluchpitts(test1,W)
        test1 = test1.flatten()
        submission.seven_segment(test1)
        j = j + 1

    submission.section("Evolving second test vector (test 2)")
    i = 1
    submission.seven_segment(test2)
    while i < 4:
        test2 = mcculluchpitts(test2,W)
        test2 = test2.flatten()
        submission.seven_segment(test2)
        i = i + 1

"""     submission.section("pretending to evolve the pattern with energies")

    test_pattern=six[:]
    
    for i in range(0,6):
        submission.seven_segment(test_pattern)
        submission.print_number(round(rnd.random(),4))
        submission.qquad()
        if (i%3)==2:
            submission.cr()
            submission.cr()
        change=rnd.randint(0,len(test_pattern)-1)
        test_pattern[change]*=-1 """

        
submission.bottomer()

    