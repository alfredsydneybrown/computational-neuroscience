w1 = weight_matrix([1 1 -1 1 1 1 1 -1 1 1 -1]); %six
w2 = weight_matrix([1 -1 1 1 -1 1 1 1 1 -1 -1]); %three
w3 = weight_matrix([-1 -1 1 -1 -1 1 -1 1 -1 -1 -1]); %one

W = (w1 + w2 + w3)./3;

test1 = [1 -1 1 1 -1 1 1 -1 -1 -1 -1];

output1 = mcculluchpitts(test1,W);

test2 = [1 1 1 1 1 1 1 -1 -1 -1 -1];

outputtest2_1 = mcculluchpitts(test2,W);

outputtest2_2 = mcculluchpitts(outputtest2_1,W);
outputtest2_3 = mcculluchpitts(outputtest2_2,W);
outputtest2_4 = mcculluchpitts(outputtest2_3,W)