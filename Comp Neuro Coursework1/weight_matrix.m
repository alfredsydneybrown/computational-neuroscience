function w_matrix = weight_matrix(vec)

    w_matrix = vec' * vec;
    
    for i = 1:length(vec)
        for j = 1:length(vec)
            if i == j
                w_matrix(i,j) = 0;
            end
        end
    end
end