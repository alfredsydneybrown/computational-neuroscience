function seven_segment(pattern)

%     function to_bool(a)
%         if a==1
%             a = true;
%         else
%             a = false;
%         end
%     end
    pattern_b = pattern == 1;
    
    hor(pattern_b(1));
    vert(pattern_b(2),pattern_b(3),pattern_b(4));
    vert(pattern_b(5),pattern_b(6),pattern_b(7));
    
    function hor(d)
        if d
            sprintf(" _ ");
        else
            sprintf("   ");
        end
    end
        
    function vert(d1,d2,d3)
        word = ""

        if d1
            word = "|"
        else
            word = " "
        end
        
        if d3
            word = word + "_"
        else
            word = word + " "
        end
        
        if d2
            word = word + "|"
        else
            word = word + " "
        end
        
        sprintf(word)

    end

%     pattern_b=list(map(to_bool,pattern))
    
    pattern_b = pattern == 1;
    
    hor(pattern_b(1))
    vert(pattern_b(2),pattern_b(3),pattern_b(4))
    vert(pattern_b(5),pattern_b(6),pattern_b(7))

    number = 0;
    for i = 0:3
        if pattern_b(8+i)
            number = number + 2^i;
    sprintf(number);
        end
    end
end