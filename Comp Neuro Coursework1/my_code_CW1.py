#Alfred Brown, ab16230, Candidate Number: 39432
#Note for the following code to work one needs to copy in the seven_segment.py code. I've just put in my work here.

#Generating weight matrices
def weight_matrix(vec):
    matrix = np.zeros((len(vec),len(vec)))
    for i in range(0,len(vec)):
        for j in range(0,len(vec)):
            if i == j:
                matrix[i][j] = 0;
            else:
                matrix[i][j] = vec[i]*vec[j]
    return matrix

#Using McCulluchPitts to evolve network
def mcculluchpitts(vec, W_matrix):
    new_vec = np.zeros((1,len(vec)))
    for i in range(0,len(vec)):
        new_vec[0][i] = np.sign(np.dot(W_matrix[i][:], vec))
    x = new_vec - vec
    return new_vec


#Getting weight matrices from training patterns:
w_one = weight_matrix([-1,-1,1,-1,-1,1,-1,1,-1,-1,-1])
w_three = weight_matrix([1,-1,1,1,-1,1,1,1,1,-1,-1])
w_six = weight_matrix([1,1,-1,1,1,1,1,-1,1,1,-1])
#Final weight matrix
W = (1/3)*(w_one + w_three + w_six)

#Test patterns
test1 =[1,-1,1,1,-1,1,1,-1,-1,-1,-1]
test2 =[1,1,1,1,1,1,1,-1,-1,-1,-1]


#Printing seven segment of pattern evolution for test pattern 1
seven_segment(test1)
j = 1
while j < 2:
    test1 = mcculluchpitts(test1,W)
    test1 = test1.flatten()
    seven_segment(test1)
    j = j + 1

#Printing seven segment of pattern evolution for test pattern 2
i = 1
seven_segment(test2)
while i < 4:
    test2 = mcculluchpitts(test2,W)
    test2 = test2.flatten()
    seven_segment(test2)
    i = i + 1