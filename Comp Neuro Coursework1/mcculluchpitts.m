function new_vec = mcculluchpitts(vec, weights_matrix)
new_vec = zeros(1,length(vec));
for i = 1:length(vec)    
    new_vec(i) = sign(sum(weights_matrix(i,:).*vec));
end

end